import argparse
from .utils import read_yaml_file
from pprint import pprint
from jinja2 import Environment, FileSystemLoader
import os
dirname = os.path.dirname(__file__)

def gen_file(input_file, ioc_name, output_file_path):
    print("Input file:", input_file)
    print("IOC name:", ioc_name)
    print("Output file path:", output_file_path)

    # read the yaml ioc deployment file
    ioc_dict = read_yaml_file(input_file)
   

    motor_ioc_dict = {}
    for ioc in ioc_dict['iocs']:
        
        if ioc['ioc_name'] == ioc_name:

            motor_ioc_dict = ioc
            break
    # Check that we found the ioc
    assert motor_ioc_dict

    # Generate the three files and put them in the desired output path
    environment = Environment(loader=FileSystemLoader(dirname + "/templates/"+motor_ioc_dict['kind']))
    #['motor.cmd', 'motor.substitutions', 'settings.req', 'aux_settings.req']
    # create required motor.cmd file
    file_path = os.path.join(dirname + "/templates/"+motor_ioc_dict['kind'])
    list_of_files = os.listdir(file_path)
    for template_name in list_of_files:
        template = environment.get_template(template_name)
        content = template.render(motor_ioc_dict)
        with open(output_file_path + "/" + template_name, mode="w", encoding="utf-8") as message:
            message.write(content)
            print(f"... wrote {output_file_path + template_name}")
 
def main():

    parser = argparse.ArgumentParser(description="Example argparse tool")
    parser.add_argument("input_file", help="Path to the input file")
    parser.add_argument("ioc_name", help="Name of the IOC")
    parser.add_argument("--output_file_path",nargs="?", const="./", default="./",help="Path to the output file")

    args = parser.parse_args()
    
    gen_file(args.input_file, args.ioc_name, args.output_file_path)

if __name__ == "__main__":
    main()