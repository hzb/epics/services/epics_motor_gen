{# templates/phytron_motor/motor.cmd #}

# Create the asyn port
drvAsynIPPortConfigure("asyn_port","$(IOC_IP):22222",0,0,1)

# Create the motor controller port
phytronCreateController ("motor_port", "asyn_port", 100, 100, 1000, 1)

# Confgiure to use faster polling
asynSetOption("motor_port",0,"pollMethod", 2)

# Create the motor controller channels
{% for channel in channels -%}
phytronCreateAxis("motor_port", {{ channel.num }},1)
{% endfor %}

#Get the extra autsave files
set_requestfile_path("$(MOTOR_PHYTRON)/db/", "")

#Final line in file. Make sure you add this....