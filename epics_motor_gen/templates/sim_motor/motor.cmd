{# templates/sim_motor/motor.cmd #}

motorSimCreateController("motor_port", {{ channels | length }})
# motorSimConfigAxis(port, axis, lowLimit, highLimit, home, start)
{% for channel in channels -%}
motorSimConfigAxis("motor_port", {{ channel.num -1}}, {{ channel.sim_hlm }}, {{ channel.sim_llm }},  {{ channel.sim_home }}, 0)
{% endfor %}
