{# templates/pigcs2_motor/motor.cmd #}

# PI GCS2 support
set_requestfile_path("$(MOTOR_PIGCS2)/db/", "")

drvAsynIPPortConfigure("asyn_port","$(IOC_IP)",0,0,0)
# Turn on asyn trace
asynSetTraceMask("asyn_port",0,3)
asynSetTraceIOMask("asyn_port",0,1)

# PI_GCS2_CreateController(portName, asynPort, numAxes, priority, stackSize, movingPollingRate, idlePollingRate)
PI_GCS2_CreateController(motor_port, "asyn_port", $(AXIS_NO) , 0,0, 100, 1000)

# Turn off asyn trace
asynSetTraceMask("asyn_port",0,1)
asynSetTraceIOMask("asyn_port",0,0)

# asyn record for troubleshooting
#dbLoadRecords("$(ASYN)/db/asynRecord.db","P=pigcs2:,R=asyn_1,PORT=C867_ETH,ADDR=0,OMAX=256,IMAX=256")


