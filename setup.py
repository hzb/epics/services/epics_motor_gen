import sys
from os import path

from setuptools import find_packages, setup

import versioneer

# NOTE: This file must remain Python 2 compatible for the foreseeable future,
# to ensure that we error out properly for people with outdated setuptools
# and/or pip.
min_version = (
    3,
    8,
)
if sys.version_info < min_version:
    error = """
epics_motor_gen does not support Python {0}.{1}.
Python {2}.{3} and above is required. Check your Python version like so:

python3 --version

This may be due to an out-of-date pip. Make sure you have pip >= 9.0.1.
Upgrade pip like so:

pip install --upgrade pip
""".format(
        *(sys.version_info[:2] + min_version)
    )
    sys.exit(error)

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as readme_file:
    readme = readme_file.read()

with open(path.join(here, "requirements.txt")) as requirements_file:
    # Parse requirements.txt, ignoring any commented-out lines.
    requirements = [line for line in requirements_file.read().splitlines() if not line.startswith("#")]


setup(
    name="epics_motor_gen",
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="A python package that takes a yaml description f an EPICS motor IOC and generates the requred st.cmd substitutions and autosave files",
    long_description=readme,
    author="Will Smith",
    author_email="william.smith@helmholtz-berlin.de",
    url="",
    python_requires=">={}".format(".".join(str(n) for n in min_version)),
    entry_points={
        "console_scripts": [
            'gen_epics_motor = epics_motor_gen.cli:main',
        ],
    },
    include_package_data=True,
    packages=['epics_motor_gen'],
    package_dir={'epics_motor_gen': 'epics_motor_gen'},
    package_data={
        "epics_motor_gen": [
            'templates/sim_motor/motor.cmd',
            'templates/sim_motor/motor.substitutions',
            'templates/sim_motor/settings.req',
            'templates/sim_motor/aux_settings.req',
            'templates/phytron_motor/motor.cmd',
            'templates/phytron_motor/motor.substitutions',
            'templates/phytron_motor/settings.req',
            'templates/phytron_motor/aux_settings.req',
            'templates/smaract_motor/smaractmcs.iocsh',
            'templates/smaract_motor/motor.substitutions.smaractmcs',
            'templates/smaract_motor/settings.req',
            'templates/smaract_motor/aux_settings.req',
            'templates/pigcs2_motor/motor.cmd',
            'templates/pigcs2_motor/motor.substitutions',
            'templates/pigcs2_motor/settings.req',
            'templates/pigcs2_motor/aux_settings.req'

            # When adding files here, remember to update MANIFEST.in as well,
            # or else they will not be included in the distribution on PyPI!
            # 'path/to/data_file',
        ]
    },
    install_requires=requirements,
    license="BSD (3-clause)",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Natural Language :: English",
        "Programming Language :: Python :: 3",
    ],
)
