# epics_motor_gen

A python package that takes a yaml description of an EPICS motor IOC and generates the requred st.cmd substitutions and autosave files

# How to Use

install the package

```
python -m pip install git+https://codebase.helmholtz.cloud/hzb/epics/services/epics_motor_gen.git
```

Create a ioc deployment yml file with a motor defined:

```yml

iocs:
  - ioc_name: sim_motor1
    kind : sim_motor
    env_vars:
      IOC_PREFIX : TESTSIMMOTOR

    
    channels:
      - { num: 1, desc: "a test motor axes", egu: "deg", dir: "Pos", velo: 1, vbas: 0.1, accl: 0.2, bdst: 0, bvel: 1, bacc: 0.2, mres: 0.01, prec: 5, dhlm: 100,dllm: -100 ,sim_llm: -10000, sim_hlm: 10000, sim_home: 5}
      - { num: 2, desc: "a test motor axes", egu: "deg", dir: "Pos", velo: 1, vbas: 0.1, accl: 0.2, bdst: 0, bvel: 1, bacc: 0.2, mres: 0.01, prec: 5, dhlm: 100,dllm: -100 ,sim_llm: -10000, sim_hlm: 10000, sim_home: 5}
      - { num: 3, desc: "a test motor axes", egu: "deg", dir: "Pos", velo: 1, vbas: 0.1, accl: 0.2, bdst: 0, bvel: 1, bacc: 0.2, mres: 0.01, prec: 5, dhlm: 100,dllm: -100 ,sim_llm: -10000, sim_hlm: 10000, sim_home: 5}

    axes:
      - { type: "passthrough",channel: 1, pv_name: "TEST_PREFIX:MOTOR1" }
      - { type: "position_saving",channel: 2, pv_name: "TEST_PREFIX:MOTOR2" }
      - { type: "position_saving",channel: 3, pv_name: "TEST_PREFIX:MOTOR3" }

    ioc_source_name: generic_motor
    cookiecutter_source_dir : /opt/epics/iocs/cookie_cutter/generic_motor
    ioc_config_source_repo : https://codebase.helmholtz.cloud/hzb/epics/ioc/config/GenericMotor-config
    ioc_config_source_tag : main
    epics_instance_dir : /opt/epics/iocs
```

You must define the `ioc_name` and `kind` must be either `sim_motor` or `phytron_motor`

Run the command line tool:

```
gen_epics_motor <path to yml file> <ioc_name> <output path>
```
The script will generate:

- motor.cmd
- motor.substitutions
- settings.req

